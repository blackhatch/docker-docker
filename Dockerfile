FROM ubuntu:disco 

LABEL Name=Docker \
        Version=edge

RUN apt update && \
        apt-get install -y curl apt-transport-https ca-certificates curl software-properties-common && \
        curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add - && \
        add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian buster stable" && \
        apt-get update && \
        apt-get install -y docker-ce && \
        apt-get clean

EXPOSE 0

CMD ["bash"]

